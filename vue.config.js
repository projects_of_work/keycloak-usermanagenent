const { defineConfig } = require('@vue/cli-service')
module.exports = defineConfig({
  transpileDependencies: [
    'vuetify'
  ]
})
module.exports = {
  devServer: {
    port: 8082,
    proxy: {
      '^/api/': {
        target: 'http://rdp.nks.com.ua:55002'
      }
    }
  }
}