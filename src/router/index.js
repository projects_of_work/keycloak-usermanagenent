import Vue from 'vue'
import VueRouter from 'vue-router'
import UsersView from '../views/UsersView.vue'
import UserRolesView from "@/views/UserRolesView";
import UserDetail from "@/components/UsersPage/UserDetailsTabs";
import AddANewUser from "@/components/UsersPage/AddANewUser";
import LandingpageView from "@/views/LandingpageView";
import UserRolesDetail from "@/components/UserRolesPage/UserRolesDetailsTabs";
import CreateUserView from "@/views/CreateUserView";
import RollenView from "@/views/RollenView";
import UsersSearchView from "@/views/UsersSearchView";

Vue.use(VueRouter)

const routes = [
  {
    path: '/',
    redirect: "/users"
  },
  {
    path: '/users',
    component: UsersView,
    children: [
      {
        path: 'details/:id',
        name: 'UsersDetailsView',
        component: UserDetail,
        props: true,
      },
      {
        path: '',
        redirect: "add"
      },
      {
        path: 'add',
        name: 'UsersMainView',
        component: AddANewUser,
      },
    ],
  },
  {
    path: '/userssearch',
    name: 'UsersSearchView',
    component: UsersSearchView,
  },
  {
    path: '/userroles',
    name: 'UserRolesView',
    component: UserRolesView,
    children: [
      {
        path: 'details/:id',
        name: 'UserRolesDetailsView',
        component: UserRolesDetail,
        props: true,
      },
    ]
  },
  {
    path: '/rollen',
    name: 'RollenView',
    component: RollenView,
  },
  {
    path: '/landingpage',
    name: 'LandingpageView',
    component: LandingpageView,
  },
  {
    path: '/createuser',
    name: 'CreateUserView',
    component: CreateUserView,
  },
]

const router = new VueRouter({
  mode: 'history',
  base: process.env.BASE_URL,
  routes
})

export default router
