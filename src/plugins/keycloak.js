import Vue from 'vue'
import Keycloak from 'keycloak-js'

const initOptions = {
    url: process.env.VUE_APP_KEYCLOAK_URL,
    realm: 'dev',
    clientId: 'dapp-client',
}

const keycloak = Keycloak(initOptions)

const KeycloakPlugin = {
    install: Vue => {
        Vue.$keycloak = keycloak
    }
}

Vue.use(KeycloakPlugin)

export default KeycloakPlugin