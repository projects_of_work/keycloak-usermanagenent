export default {
    state: {
        messageCount: 2,
    },
    getters: {
        messageCount(state) {
            return state.messageCount;
        }
    },
    mutations: {

    },
    actions: {

    }
}