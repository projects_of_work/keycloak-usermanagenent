import axios from "axios";

export default {
    state: {
        userInfo: {},
        userRoles: [
            {
                name: "Card Management",
                sAa: ".CARD",
                assignedOn: "12.12.2021",
                assignedBy: "Max Mustermann",
            },
            {
                name: "DIM Management",
                sAa: "#Identity",
                assignedOn: "12.12.2021",
                assignedBy: "Max Mustermann",
            },
        ],
    },
    getters: {
        currentUserInfo(state) {
            return state.userInfo;
        },
        currentUserRolesInfo(state) {

            console.log("roles2", state.userRoles)
            return state.userRoles;
        }
    },
    mutations: {
        updateUserInfo(state, user) {
            state.userInfo = user;
        },
        updateUserRoles(state, roles) {
            state.userRoles = roles;
        }
    },
    actions: {
        async GetUserInfo(context, id) {
            let user;
            await axios
                .get(`/api/users/item/?Id=${id}`)
                .then((response) => (user = response.data))
                .catch((error) => console.log(error));
            context.commit('updateUserInfo', user);
            console.log(user);
            let roles;
            await axios
                .get(`/api/roles/user-roles/?UserId=${id}`)
                .then((response) => (roles = response.data.roles))
                .catch((error) => console.log(error));
            context.commit('updateUserRoles', roles);
            console.log(roles);
        }
    }
}