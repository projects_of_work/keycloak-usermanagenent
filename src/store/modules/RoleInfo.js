import axios from "axios";

export default {
    state: {
        roleInfo: {},
        roleUsers: [
            /*{
                lastname: "Adler",
                firstname: "Tim",
                otherRoles: "Onboarding Manager",
                assignedOn: "12.12.2020",
            },
            {
                lastname: "Adler",
                firstname: "Tim",
                otherRoles: "Onboarding Manager",
                assignedOn: "12.12.2020",
            },
            {
                lastname: "Adler",
                firstname: "Tim",
                otherRoles: "Onboarding Manager",
                assignedOn: "12.12.2020",
            },*/
        ],
    },
    getters: {
        currentRoleInfo(state) {
            return state.roleInfo;
        },
        currentRoleUsersInfo(state) {
            return state.roleUsers;
        }
    },
    mutations: {
        updateRoleInfo(state, role) {
            state.roleInfo = role;
        },
        updateRoleUsers(state, users) {
            state.roleUsers = users;
        }
    },
    actions: {
        async GetRoleInfo(context, id) {
            let role;
            await axios
                .get(`/api/roles/item/?Id=${id}`)
                .then((response) => (role = response.data))
                .catch((error) => console.log(error));
            context.commit('updateRoleInfo', role);
            console.log(role);
            let users;
            await axios
                .get(`/api/roles/assigned-users/?RoleId=${id}`)
                .then((response) => (users = response.data.users))
                .catch((error) => console.log(error));
            context.commit('updateRoleUsers', users);
            console.log(users)
        }
    }
}