import axios from "axios";

export default {
    state: {
        ApplicationsAndSystemsAndRolesListItems: [],
    },
    getters: {
        AllApplicationsAndSystemsAndRoles(state) {
            let tempArray = [];
            for (const item of state.ApplicationsAndSystemsAndRolesListItems) {
                let tempObj = {
                    id: item.id,
                    name: item.name,
                    children: [
                    ]
                };
                for (const role of item.roles) {
                    tempObj.children.push(
                        {
                            id: role.id,
                            name: role.name,
                            appAndSys: item.name,
                        }
                    )
                }
                tempArray.push(tempObj);
            }
            return tempArray;
        },
        allApplicationsAndSystems(state) {
            return state.ApplicationsAndSystemsAndRolesListItems;
        }
    },
    mutations: {
        updateApplicationsAndSystemsAndRolesListItems(state, appAndSys) {
            state.ApplicationsAndSystemsAndRolesListItems = appAndSys;
        }
    },
    actions: {
        async GetApplicationsAndSystemsAndRolesList(context) {
            let appAndSys;
            await axios
                .get("/api/roles/app-and-sys")
                .then((response) => (appAndSys = response.data.appAndSyss))
                .catch((error) => console.log(error));
            context.commit('updateApplicationsAndSystemsAndRolesListItems', appAndSys);
            console.log("appandsys", appAndSys);
        }
    }
}