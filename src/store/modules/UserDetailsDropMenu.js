export default {
    state: {
        itemsUserDetailsDropMenu: [
            { title: 'Bearbeiten', icon: 'mdi-square-edit-outline' },
            { title: 'Berechtigte', icon: 'mdi-account-lock-open-outline' },
            { title: 'Berechtigungen', icon: 'mdi-lock-open-variant-outline' },
            { title: 'Historie', icon: 'mdi-history' },
        ],
    },
    getters: {
        allItemsUserDetailsDropMenu(state) {
            return state.itemsUserDetailsDropMenu;
        }
    },
    mutations: {

    },
    actions: {

    }
}