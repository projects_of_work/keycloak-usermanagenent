import axios from "axios";

export default {
    state: {
        surnameInputValue: "",
        firstnameInputValue: "",
        usernameInputValue: "",
        emailInputValue: "",
        passwordInputValue: "",
        confPasswordInputValue: "",
        ApplicationsAndSystemsAndRolesSelection: [],
    },
    getters: {
        AllApplicationsAndSystemsAndRolesSelection(state) {
            return state.ApplicationsAndSystemsAndRolesSelection;
        }
    },
    mutations: {

    },
    actions: {
        async PostCreateUser(context) {
            let tempRolesArray = [];
            for (const item of this.state.ApplicationsAndSystemsAndRolesSelection) {
                tempRolesArray.push(item.id);
            }
            await axios
                .post("/api/users/add-user", {
                    userName: this.state.usernameInputValue,
                    firstName: this.state.firstnameInputValue,
                    lastName: this.state.surnameInputValue,
                    email: this.state.emailInputValue,
                    password: this.state.passwordInputValue,
                    rolesId: tempRolesArray,
                })
                .then((response) => {
                    console.log(response);
                    if (response.status === 200)
                        alert("User successfully created");
                    else
                        alert("An error occurred, please try again later");
                })
                .catch((error) => console.log(error));
        }
    }
}