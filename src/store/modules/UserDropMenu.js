export default {
    state: {
        itemsUserDropMenu: [
            {title: 'Bearbeiten', icon: 'mdi-square-edit-outline'},
            {title: 'Kennwort zurucksetzen', icon: 'mdi-account-lock-open-outline'},
            {title: 'Rollen', icon: 'mdi-account-cog-outline'},
            {title: 'Histoie', icon: 'mdi-history'},
            {title: 'Benutzer loschen', icon: 'mdi-trash-can-outline'},
        ],
    },
    getters: {
        allItemsUserDropMenu(state) {
            return state.itemsUserDropMenu;
        }
    },
    mutations: {

    },
    actions: {

    }
}