import axios from "axios";

export default {
    state: {
        userListItems: [
        ]
    },
    getters: {
        allUserlistItems(state) {
            let tempArray = [];
            for (const item of state.userListItems) {
                let tempObj = Object.assign({}, item);
                tempObj.initials = item.firstName[0] + item.lastName[0];
                tempArray.push(tempObj);
            }
            return tempArray;
        },
    },
    mutations: {
        updateUserListItems(state, users) {
            state.userListItems = users;
        }
    },
    actions: {
        async GetUserList(context) {
            let users;
            await axios
                .get("/api/users/list")
                .then((response) => (users = response.data.users))
                .catch((error) => console.log(error));
            context.commit('updateUserListItems', users);
            console.log(users);
        }

    }
}
