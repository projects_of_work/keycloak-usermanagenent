import axios from "axios";

export default {
    state: {
        roleListItems: [
        ]
    },
    getters: {
        allRolelistItems(state) {
            console.log(state.roleListItems)
            return state.roleListItems;
        },
    },
    mutations: {
        updateRoleListItems(state, roles) {
            state.roleListItems = roles;
        }
    },
    actions: {
        async GetRoleList(context) {
            let roles;
            await axios
                .get("/api/roles/list")
                .then((response) => (roles = response.data.roles))
                .catch((error) => console.log(error));
            context.commit('updateRoleListItems', roles);
        }
    }
}