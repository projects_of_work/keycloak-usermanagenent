export default {
    state: {
        navbarItems: [
            {
                title: 'Landingpage',
                icon: 'mdi-speedometer',
                pathName: 'LandingpageView',
            },
            {
                title: 'Users',
                icon: 'mdi-account-lock-open-outline',
                pathName: 'UsersMainView',
            },
            {
                title: 'User Roles',
                icon: 'mdi-lock-open-variant-outline',
                pathName: 'UserRolesView',
            },
        ]
    },
    getters: {
        allNavbarItems(state) {
            return state.navbarItems;
        }
    },
    mutations: {

    },
    actions: {

    }
}