export default {
    state: {
        searchField: "",
        itemsSearchDropMenu: [
            { title: 'Weitere Filter', icon: 'mdi-filter-outline' },
            { title: 'Benutzer importieren', icon: 'mdi-file-move-outline' },
            { title: 'Selection aktivieren', icon: 'mdi-checkbox-outline' },
        ],
    },
    getters: {
        allItemsSearchDropMenu(state) {
            return state.itemsSearchDropMenu;
        }
    },
    mutations: {

    },
    actions: {

    }
}