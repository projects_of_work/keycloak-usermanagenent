import Vue from 'vue';
import Vuex from 'vuex';
import Navbar from './modules/Navbar';
import Messages from './modules/Messages';
import UserList from "@/store/modules/UserList";
import UserRolesList from "@/store/modules/UserRolesList";
import Search from "@/store/modules/Search";
import UserDropMenu from "@/store/modules/UserDropMenu";
import UserDetailsDropMenu from "@/store/modules/UserDetailsDropMenu";
import UserInfo from "@/store/modules/UserInfo";
import RoleInfo from "@/store/modules/RoleInfo";
import ApplicationsAndSystemsList from "@/store/modules/ApplicationsAndSystemsAndRolesList";
import CreateUserForm from "@/store/modules/CreateUserForm";

Vue.use(Vuex)

export default new Vuex.Store({
  modules: {
    Navbar,
    UserList,
    UserRolesList,
    Search,
    UserDropMenu,
    UserDetailsDropMenu,
    Messages,
    UserInfo,
    RoleInfo,
    ApplicationsAndSystemsList,
    CreateUserForm,
  }
})
