import Vue from 'vue'
import App from './App.vue'
import router from './router'
import store from './store'
import vuetify from './plugins/vuetify'
import '@/plugins/keycloak'
import { updateToken } from '@/plugins/keycloak-util'

Vue.config.productionTip = false

Vue.$keycloak.init({ onLoad: 'login-required' }).then((auth) => {
  if (!auth) {
    window.location.reload();
  } else {
    new Vue({
      router,
      store,
      vuetify,
      render: h => h(App)
    }).$mount('#app')

    window.onfocus = () => {
      updateToken()
    }
  }
})

